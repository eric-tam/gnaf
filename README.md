# NuSkope MySQL G-NAF Tools #

These tools are for manipulating G-NAF addressing data provided by PSMA.

## MySQL Import ##

To import data into MySQL, or MariaDB:

```
unzip feb17-gnaf-pipeseperatedvalue.zip
./import-mysql | mysql -u root GNAF_201702
```

## License ##

This code is not covered by warranty or license. It is shared in the 
public domain.
